@extends('layouts.app')

@section('title', 'Dashboard')

@section('content_header')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="card-title">
                                <i class="fas fa-edit"></i>
                            </h3>
                            <button type="button" class="btn btn-outline-primary ml-3" data-toggle="modal"
                                    data-target="#modal-user">
                                New User
                            </button>
                        </div>
                        <div class="card-body">
                            @include('user.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('user.create')
    @include('user.edit')
@stop
