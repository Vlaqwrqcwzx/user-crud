<div class="box">
    <div class="box-body" style="overflow: hidden">
        <table class="table table-striped table-bordered table-hover dataTable no-footer" id="data-table"
               style="width:100%;">
            <thead>
            @if(!empty($columnList))
                <tr>
                    @foreach ($columnList as $columnData)
                        <th>{{ $columnData['caption'] }}</th>
                    @endforeach
                </tr>
            @endif
            </thead>
        </table>
    </div>
</div>
