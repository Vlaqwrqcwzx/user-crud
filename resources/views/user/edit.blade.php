<div class="modal fade" id="modal-user-edit" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit User <span class="edited-user-name"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="editUser">
                <input type="text" id="routeUpdate" hidden>
                <input name="_method" type="hidden" value="PATCH">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="edit-name">Name</label>
                        <input type="text" name="name" class="form-control" id="edit-name" placeholder="name" required>
                    </div>
                    <div class="form-group">
                        <label for="edit-email">Email</label>
                        <input type="email" name="email" class="form-control" id="edit-email" placeholder="Enter email" required>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" id="resetPassword" onclick="App.resetPassword(this)"
                                   name="resetPassword">
                            <label for="resetPassword" class="custom-control-label">Change
                                Password</label>
                        </div>
                    </div>
                    <div class="password-group">
                    </div>
                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" name="role_id" id="edit-role" required>

                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
