@extends('adminlte::page')

@section('adminlte_css')
    <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/toastr/toastr.min.css">
@stop

@section('adminlte_js')
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script>
        var GConfig = {
            'userStore': '{{ route('users.store') }}',
            'usersIndex': '{{ route('users.index') }}',
            'token': '{!! csrf_token() !!}',
        }
    </script>
    <script src="{{ asset('js/user.js') }}"></script>
@stop
