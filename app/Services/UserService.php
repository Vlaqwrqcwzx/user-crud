<?php

namespace App\Services;

use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use function GuzzleHttp\Promise\all;

class UserService
{
    private $columnList;


    public function __construct()
    {
        $this->columnList = [
            'id' => [
                'caption' => 'id',
                'name' => 'id',
            ],
            'name' => [
                'caption' => 'name',
                'name' => 'name',
            ],
            'email' => [
                'caption' => 'email',
                'name' => 'email',
            ],
            'actions' => [
                'caption' => 'Actions',
                'orderable' => 'false',
            ],
        ];
    }

    public function getDataTable()
    {
        $users = User::latest()->get();

        $dataTable = Datatables::of($users)
            ->addColumn('id', function ($row) {
                return $row->id;
            })
            ->addColumn('name', function ($row) {
                return $row->name;
            })
            ->addColumn('email', function ($row) {
                return $row->email;
            })
            ->addColumn('action', function ($user) {
                return '
                    <div class="d-flex">
                        <a
                            href="javascript:void(0);"
                            onclick="App.editUserFromTable(\'' . route('users.edit', $user) . '\');"
                            class="btn btn-sm btn-outline-primary">
                            <i class="fas fa-edit"></i>
                            Edit
                        </a>
                        <a
                            onclick="App.deleteUserFromTable(\'' . route('users.destroy', $user) . '\');"
                            href="javascript:void(0);"
                            class="btn btn-sm btn-outline-danger ml-1">
                            <i class="fas fa-trash"></i>
                            Delete
                        </a>
                    </div>
                ';
            })
            ->removeColumn('password');

        return $dataTable->make(true);
    }

    public function viewElementList()
    {
        $elementListConfig = [
            'controller' => 'UserController',
            'func_to_list' => 'index',
            'func_to_create' => 'create'
        ];
        $roles = Role::all();
        return view('user.index', [
            'columnList' => $this->columnList,
            'roles' => $roles,
            'elementListConfig' => $elementListConfig
        ]);

    }

    public function store($request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $role = Role::find($request->role_id);
        $user->attachRole($role);

        return $user;
    }

    public function edit($request, $user)
    {
        if ($request->ajax()) {

            $userRoles = [];
            $roles = Role::all();

            foreach ($roles as $role) {
                $userRoles[] = [
                    'id' => $role->id,
                    'display_name' => $role->display_name,
                    'hasRole' => $user->hasRole($role->name)
                ];
            }

            return response()->json([
                'userRoles' => $userRoles,
                'user' => $user,
                'routeUpdate' => route('users.update', $user->id)
            ]);
        }
        abort(404);
    }

    public function update($request, $user)
    {
        if ($request->ajax()) {
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
                'password' => 'string|min:8|confirmed',
            ]);

            if (isset($request->password)) {
                $user->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                ]);
            } else {
                $user->update([
                    'name' => $request->name,
                    'email' => $request->email,
                ]);
            }

            $user->detachAllRoles();
            $role = Role::find($request->role_id);
            $user->attachRole($role);
        }
        return response()->json(['message' => 'ok']);
    }
}
