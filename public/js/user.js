var App = {
    // tostr settings
    tostr: $(function () {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });
    }),

    // datatable settings
    table: $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        ajax: GConfig.usersIndex,
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    }),

    editUserFromTable: function (routeEdit) {
        $.ajax({
            type: 'GET',
            url: routeEdit,
            dataType: 'json',
            data: {
                _token: GConfig.token
            },
            success: function (data) {
                $('#edit-name').val(data.user['name'])
                $('#edit-email').val(data.user['email'])
                $('#edit-role').empty()
                $.each(data.userRoles, function (index, value) {
                    $('#edit-role').append(new Option(value["display_name"], value["id"], false, value['hasRole']))
                });
                $('#routeUpdate').val(data.routeUpdate)
                $('#modal-user-edit').modal('show')
            }
        });
    },

    deleteUserFromTable: function (routeDelete) {
        if (confirm('Delete user?')) {
            $.ajax({
                type: 'POST',
                url: routeDelete,
                dataType: 'json',
                data: {
                    _method: 'DELETE',
                    _token: GConfig.token
                },
                success: function (data) {
                    toastr.success('User was deleted')
                    App.table.ajax.reload(null, false)
                }
            });
        }
    },

    userAdd: function (data) {
        $.ajax({
            type: 'POST',
            url: GConfig.userStore,
            data: data,
            success: function (data) {
                App.table.ajax.reload(null, false)
                toastr.success('User was created')
                $("#modal-user").modal('hide')
                jQuery('#newUser')[0].reset();
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText)
                $.each(errors.errors, function (index, value) {
                    toastr.error(value)
                });
            }
        });
    },

    userUpdate: function () {
        var routeUpdate = $('#routeUpdate').val()
        var data = $('#editUser').serialize();

        $.ajax({
            type: 'POST',
            url: routeUpdate,
            data,
            success: function (data) {
                console.log(data)
                $('#modal-user-edit').modal('hide')
                toastr.success('User updated')
                App.table.ajax.reload(null, false)
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText)
                $.each(errors.errors, function (index, value) {
                    toastr.error(value)
                });
            }
        })


    },

    resetPassword: function (checkbox) {
        if (checkbox.checked) {
            $('.password-group').append('' +
                '<div class="form-group">' +
                '<label for="password">New password</label>' +
                '<input type="password" name="password" class="form-control"' +
                'id="exampleInputPassword1"' +
                'placeholder="New password" required>' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="password_confirmation">Confirm New password</label>' +
                '<input type="password" name="password_confirmation" class="form-control"' +
                'id="password_confirmation"' +
                'placeholder="Confirm New password" required>' +
                '</div>');
        } else {
            $('.password-group').empty();
        }
    }
}

$(document).ready(function () {
    // add User
    $('#newUser').submit(function (e) {
        e.preventDefault()
        App.userAdd($(this).serialize())
    })

    // update User
    $('#editUser').submit(function (e) {
        e.preventDefault()
        App.userUpdate()
    })

});
