<?php

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RolePermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create([
            'name' => 'admin',
            'display_name' => 'User Administrator',
            'description' => 'User is allowed crud other users',
        ]);

        $guest = Role::create([
            'name' => 'guest',
            'display_name' => 'User Guest',
            'description' => 'Just guest',
        ]);


        $createUser = Permission::create([
            'name' => 'create-user',
            'display_name' => 'Create User',
            'description' => 'Create new user',
        ]);

        $updateUser = Permission::create([
            'name' => 'update-user',
            'display_name' => 'Update Users',
            'description' => 'Update existing users',
        ]);

        $editUser = Permission::create([
            'name' => 'edit-user',
            'display_name' => 'Edit Users',
            'description' => 'edit existing users',
        ]);

        $destroyUser = Permission::create([
            'name' => 'destroy-user',
            'display_name' => 'Destroy Users',
            'description' => 'Destroy existing users',
        ]);

        $admin->attachPermission($createUser);
        $admin->attachPermission($updateUser);
        $admin->attachPermission($editUser);
        $admin->attachPermission($destroyUser);

        $user = User::find(1);
        $user->attachRole($admin);
    }
}
